# Molarità Morlacchi

## App compilata solo per Android

### Su smartphone
	
	Apri l'App 'Molarità Morlacchi', premendo 'Tutorial' si aprirà il tutorial per l'utilizzo dell'app.
	L'app è in grado di eseguire i calcoli per trovare la molarità e la mole data la massa e la Massa Molare (formule nella sezione 'Formule' dell'app)
	Se i valori inseriti non rispettano le equazioni verranno corretti automaticamente
	
### Desktop

	Per eseguire l'app su computer:
	Apri il file www/index.html

	

